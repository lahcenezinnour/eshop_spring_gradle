-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema ecommerce
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema ecommerce
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `ecommerce` DEFAULT CHARACTER SET utf8 ;
USE `ecommerce` ;

-- -----------------------------------------------------
-- Table `ecommerce`.`options`
-- -----------------------------------------------------

-- CREATE TABLE IF NOT EXISTS `ecommerce`.`options` (
--   `OptionID` INT(11) NOT NULL AUTO_INCREMENT,
--   `OptionName` VARCHAR(50) NULL DEFAULT NULL,
--   `OptionColor` set('Beige','Blue','Green','Honeydew','Metallics','Navy','Orange','Purple','White','Black','Brown','Grey','Khaki','Multi','Neon','Pink','Red','Yellow') DEFAULT NULL,
--   `OptionSize` set('M','XL4','L','XL','XXL','XXXL','S','XXS','XS') DEFAULT NULL,
--   `OptionSizeNum` INT(11) DEFAULT NULL,
--   `OptionBand` set('NIKE','ADIDAS') DEFAULT NULL,
--   PRIMARY KEY (`OptionID`),
--   FOREIGN KEY (`OptionID`) REFERENCES `ecommerce`.`products` (`ProductID`));

--
-- Table structure for table `optiongroups`
--

CREATE TABLE IF NOT EXISTS `optiongroups` (
  `OptionGroupID` int(11) NOT NULL AUTO_INCREMENT,
  `OptionGroupName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`OptionGroupID`));

INSERT INTO `optiongroups` (`OptionGroupID`, `OptionGroupName`) VALUES
(1, 'color'),
(2, 'size'),
(3, 'brand'),
(4, 'gender');

--
-- Table structure for table `options`
--

CREATE TABLE IF NOT EXISTS `options` (
  `OptionID` int(11) NOT NULL AUTO_INCREMENT,
  `OptionGroupID` int(11) NOT NULL,
  `OptionName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`OptionID`),
  FOREIGN KEY (`OptionGroupID`) REFERENCES `optiongroups` (`OptionGroupID`) ON DELETE CASCADE);

INSERT INTO `options` (`OptionID`, `OptionGroupID`, `OptionName`) VALUES

(1, 1, 'Assorted'),
(2, 1, 'Beige'),
(3, 1, 'Black'),
(4, 1, 'Blue'),
(5, 1, 'Brown'),
(6, 1, 'Crimson'),
(7, 1, 'Green'),
(8, 1, 'Grey'),
(9, 1, 'Orange'),
(10, 1, 'Pink'),
(11, 1, 'Red'),
(12, 1, 'White'),
(13, 1, 'Yellow'),
(14, 2, 'one size'),
(15, 2, 'x-small'),
(16, 2, 'small'),
(17, 2, 'sml-med'),
(18, 2, 'medium'),
(19, 2, 'large'),
(20, 2, 'large-xl'),
(21, 2, 'x-large'),
(22, 2, 'xx-large'),
(23, 3, 'Le 31'),
(24, 3, 'Djab'),
(25, 3, 'BOSS Orange'),
(26, 3, 'Blend'),
(27, 3, 'Element'),
(28, 3, 'Fairplay'),
(29, 3, 'Fileuse d arvor'),
(30, 3, 'G-Star Raw'),
(31, 3, 'Hurley'),
(32, 3, 'Imperial'),
(33, 3, 'Jack & Jones'),
(34, 3, 'John Varvatos'),
(35, 3, 'Lacoste'),
(36, 3, 'Michael Kors'),
(37, 3, 'Obey'),
(38, 3, 'Only & Sons'),
(39, 3, 'Point Zero'),
(40, 3, 'Private Member'),
(41, 3, 'Projek Raw'),
(42, 3, 'Rumors'),
(43, 3, 'Selected'),
(44, 3, 'Tiger of Sweden'),
(45, 3, 'Vans'),
(46, 3, 'Vitaly'),
(47, 3, 'Volcom'),
(48, 4, 'Man'),
(49, 4, 'Woman');

--
-- Table structure for table `productoptions`
--

CREATE TABLE IF NOT EXISTS `productoptions` (
  `ProductOptionID` int(10) NOT NULL AUTO_INCREMENT,
  `ProductID` int(10) NOT NULL,
  `OptionID` int(10) NOT NULL,
  `OptionPriceIncrement` double DEFAULT NULL,
  `OptionGroupID` int(11) NOT NULL,
  PRIMARY KEY (`ProductOptionID`),
  FOREIGN KEY (`ProductID`) REFERENCES `products` (`ProductID`) ON DELETE CASCADE,
  FOREIGN KEY (`OptionID`) REFERENCES `options` (`OptionID`) ON DELETE CASCADE);

INSERT INTO `productoptions` (`ProductOptionID`, `ProductID`, `OptionID`, `OptionPriceIncrement`, `OptionGroupID`) VALUES
(1, 1, 1, 0, 1),
(2, 1, 2, 0, 1),
(3, 1, 3, 0, 1),
(4, 1, 4, 0, 2),
(5, 1, 5, 0, 2),
(6, 1, 6, 0, 2),
(7, 1, 7, 2, 2),
(8, 1, 8, 2, 2);


-- -----------------------------------------------------
-- Table `ecommerce`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce`.`users` (
  `UserID` INT(11) NOT NULL AUTO_INCREMENT,
  `UserEmail` LONGTEXT NULL DEFAULT NULL,
  `UserFirstName` VARCHAR(50) NULL DEFAULT NULL,
  `UserLastName` VARCHAR(50) NULL DEFAULT NULL,
  `UserPassword` LONGTEXT NULL DEFAULT NULL,
  PRIMARY KEY (`UserID`));

-- -----------------------------------------------------
-- Table `ecommerce`.`customer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce`.`customer` (
  `UserID` INT(11) NOT NULL AUTO_INCREMENT,
  `UserAddress` VARCHAR(100) NULL DEFAULT NULL,
  `UserAddress2` VARCHAR(50) NULL DEFAULT NULL,
  `UserCity` VARCHAR(90) NULL DEFAULT NULL,
  `UserCountry` VARCHAR(20) NULL DEFAULT NULL,
  `UserEmailVerified` BIT(1) NULL DEFAULT NULL,
  `UserPhone` VARCHAR(20) NULL DEFAULT NULL,
  `UserRegistrationDate` DATETIME NULL DEFAULT NULL,
  `UserState` VARCHAR(20) NULL DEFAULT NULL,
  `UserVerificationCode` VARCHAR(20) NULL DEFAULT NULL,
  `UserZip` VARCHAR(12) NULL DEFAULT NULL,
  PRIMARY KEY (`UserID`),
  FOREIGN KEY (`UserID`) REFERENCES `ecommerce`.`users` (`UserID`));


-- -----------------------------------------------------
-- Table `ecommerce`.`orders`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce`.`orders` (
  `OrderID` INT(11) NOT NULL AUTO_INCREMENT,
  `OrderAmount` FLOAT NOT NULL,
  `OrderCity` VARCHAR(50) NOT NULL,
  `OrderCountry` VARCHAR(50) NOT NULL,
  `OrderDate` DATETIME NOT NULL,
  `OrderEmail` VARCHAR(100) NOT NULL,
  `OrderFax` VARCHAR(20) NOT NULL,
  `OrderPhone` VARCHAR(20) NOT NULL,
  `OrderShipAddress` VARCHAR(100) NOT NULL,
  `OrderShipAddress2` VARCHAR(100) NOT NULL,
  `OrderShipName` VARCHAR(100) NOT NULL,
  `OrderShipped` BIT(1) NOT NULL,
  `OrderShipping` FLOAT NOT NULL,
  `OrderState` VARCHAR(50) NOT NULL,
  `OrderTax` FLOAT NOT NULL,
  `OrderTrackingNumber` VARCHAR(80) NULL DEFAULT NULL,
  `OrderZip` VARCHAR(20) NOT NULL,
  `OrderUserID` INT(11) NOT NULL,
  PRIMARY KEY (`OrderID`),
  FOREIGN KEY (`OrderUserID`) REFERENCES `ecommerce`.`customer` (`UserID`));


-- -----------------------------------------------------
-- Table `ecommerce`.`productcategories`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce`.`productcategories` (
  `CategoryID` INT(11) NOT NULL AUTO_INCREMENT,
  `CategoryName` VARCHAR(50) NOT NULL,
  `CategoryGender` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`CategoryID`));

INSERT INTO `ecommerce`.`productcategories` (`CategoryName`, `CategoryGender`) 
VALUES 
-- ----------------------------- MEN
('Sweaters & Cardigans', 'MEN'),('Coats & Outerwear', 'MEN'),('Casual shirts', 'MEN'),
('T-Shirts, Tanks & Polos', 'MEN'),('Sweatshirts & Hoodies', 'MEN'),('Denim', 'MEN'),('Pants', 'MEN'),
('Shorts & Capris', 'MEN'),('Suits', 'MEN'),('Jackets', 'MEN'),('Dress shirts', 'MEN'),
('Ties & Bow ties', 'MEN'),('Swimwear', 'MEN'),
-- ----------------------------- WOMEN
('Coats', 'WOMEN'),('Sweaters & Cardigans', 'WOMEN'),('T-Shirts', 'WOMEN'),('Dresses', 'WOMEN'),
('Blouses & Shirts', 'WOMEN'),('Sweatshirts & hoodies', 'WOMEN'),('Jackets', 'WOMEN'),('Pants', 'WOMEN'),
('Jeans', 'WOMEN'),('Skirts', 'WOMEN'),('Shorts', 'WOMEN'),('Beach & Swimwear', 'WOMEN');


-- -----------------------------------------------------
-- Table `ecommerce`.`subproductcategories`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce`.`subproductcategories` (
  `SubCategoryID` INT(11) NOT NULL AUTO_INCREMENT,
  `SubCategoryName` VARCHAR(150) NOT NULL,
  `CategoryID` INT(11) NOT NULL,
  PRIMARY KEY (`SubCategoryID`),
  FOREIGN KEY (`CategoryID`) REFERENCES `ecommerce`.`productcategories` (`CategoryID`));

INSERT INTO `ecommerce`.`subproductcategories` (`SubCategoryName`, `CategoryID`) 
VALUES 

-- ----------------------------- Men
-- ----------------------------- Sweaters & Cardigans
('Crew necks', '1'),('V-necks', '1'),('Turtlenecks & Mock necks', '1'),('Shawls & Cardigans', '1'),
('Hooded', '1'),
('Lambswool', '1'),('Merino Wool', '1'),('Cotton', '1'),('Cashmere', '1'),('Fine Cotton Knit', '1'),
('Blends', '1'),

-- ----------------------------- Coats & Outerwear
('Curated Collections', '2'),('Parkas & Down', '2'),('Jackets & Vests', '2'),('Overcoats', '2'),
('Leather & Suede', '2'),('Raincoats & Windbreakers', '2'),('Technical sports', '2'),

-- ----------------------------- Casual shirts
('Short sleeves', '3'),('Long sleeves', '3'),('Pure Linen', '3'),('Checks', '3'),('Patterns', '3'),('Solid', '3'),

-- ----------------------------- T-Shirts, Tanks & Polos
('Short sleeves & 3/4 sleeves', '4'),('Polos', '4'),('Prints', '4'),('Logo wear', '4'),('Buttoned necks', '4'),
('Long sleeves', '4'),

-- ----------------------------- Sweatshirts & Hoodies

-- ----------------------------- Denim
('Super skinny & Skinny fit', '6'),('Slim fit', '6'),('Straight fit', '6'),('Premium Denim', '6'),
('New Proportions', '6'),

-- ----------------------------- Pants
('Tailored', '7'),('Joggers', '7'),('Super skinny & Skinny fit', '7'),('Slim fit', '7'),('Straight fit', '7'),
('New Proportions', '7'),

-- ----------------------------- Shorts & Capris
('Bermudas', '8'),('Long bermudas', '8'),('Capris', '8'),

-- ----------------------------- Suits
('Slim Fit', '9'),('Semi-slim Fit', '9'),('Regular Fit', '9'),

-- ----------------------------- Jackets
('Slim Fit', '10'),('Semi-slim Fit', '10'),('Regular Fit', '10'),

-- ----------------------------- Dress shirts
('Tailored fit', '11'),('Semi-tailored fit', '11'),('Regular fit', '11'),('Easy Care', '11'),

-- ----------------------------- Ties & Bow ties
('Regular Ties', '12'),('Skinny Ties', '12'),('Bow Ties', '12'),('Pocket Squares & Scarves', '12'),

-- ----------------------------- Swimwear

-- ----------------------------- WOMEN
-- ----------------------------- Coats
('Anoraks and Parkas', '14'),('Quilted and Down', '14'),('Wool', '14'),('Trenches and Raincoats', '14'),
('Jackets and Vests', '14'),('Leather and Suede', '14'),('Sports Performance', '14'),

-- ----------------------------- Sweaters & Cardigans
('Sweaters', '15'),('Cardigans', '15'),('Tunics', '15'),('Turtlenecks & Mock necks', '15'),
('Capes & Ponchos', '15'),

-- ----------------------------- T-Shirts
('Tank Tops', '16'),('Short Sleeves', '16'),('Long Sleeves', '16'),('Crop Tops', '16'),('Tunics', '16'),

-- ----------------------------- Dresses
('Mini', '17'),('Knee', '17'),('Midi', '17'),('Maxi', '17'),('Dressy', '17'),('Career', '17'),('Casual', '17'),
('Casual', '17'),('Little black dress', '17'),('Jumpsuits', '17'),

-- ----------------------------- Blouses & Shirts
('Chic', '18'),('Fluid', '18'),('Classic', '18'),('Casual', '18'),('Crop Tops', '18'),('Tunics', '18'),

-- ----------------------------- Sweatshirts & hoodies

-- ----------------------------- Jackets

-- ----------------------------- Pants
('Pants', '21'),('Leggings', '21'),('Fluid', '21'),('Tailored', '21'),('Casual', '21'),

-- ----------------------------- Jeans
('Boyfriend', '22'),('Skinny leg', '22'),('Straight leg', '22'),('Boot cut leg', '22'),('Premium denim', '22'),
('Overalls', '22'),

-- ----------------------------- Skirts
('Mini', '23'),('Knee', '23'),('Midi', '23'),('Maxi', '23'),

-- ----------------------------- Shorts

-- ----------------------------- Beach & Swimwear
('Tops', '25'),('Bottoms', '25'),('One-Pieces & Tankinis', '25'),('Rashguards', '25'),('D Cup', '25'),
('Collections', '25'),('Beach Cover-ups & Accessories', '25');


-- -----------------------------------------------------
-- Table `ecommerce`.`products`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce`.`products` (
  `ProductID` INT(11) NOT NULL AUTO_INCREMENT,
  `ProductCartDesc` VARCHAR(250) NULL DEFAULT NULL,
  `ProductImage` VARCHAR(250) NULL DEFAULT NULL,
  `ProductLive` BIT(1) NULL DEFAULT NULL,
  `ProductLocation` VARCHAR(250) NULL DEFAULT NULL,
  `ProductLongDesc` LONGTEXT NULL DEFAULT NULL,
  `ProductName` VARCHAR(100) NOT NULL,
  `ProductPrice` FLOAT NOT NULL,
  `ProductShortDesc` LONGTEXT NULL DEFAULT NULL,
  `ProductSKU` VARCHAR(50) NULL DEFAULT NULL,
  `ProductStock` FLOAT NULL DEFAULT NULL,
  `ProductThumb` VARCHAR(100) NULL DEFAULT NULL,
  `ProductUnlimited` BIT(1) NULL DEFAULT NULL,
  `ProductUpdateDate` DATETIME NULL DEFAULT NULL,
  `ProductWeight` FLOAT NULL DEFAULT NULL,
  `ProductActivityDisplay` VARCHAR(6) NULL DEFAULT NULL,
  `ProductActivityName` VARCHAR(20) NULL DEFAULT NULL,
  `ProductCategoryID` INT(11) NOT NULL,
  PRIMARY KEY (`ProductID`),
  FOREIGN KEY (`ProductCategoryID`) REFERENCES `ecommerce`.`productcategories` (`CategoryID`));


-- -----------------------------------------------------
-- Table `ecommerce`.`orderdetails`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce`.`orderdetails` (
  `DetailID` INT(11) NOT NULL AUTO_INCREMENT,
  `DetailName` VARCHAR(250) NOT NULL,
  `DetailPrice` FLOAT NOT NULL,
  `DetailQuantity` INT(11) NOT NULL,
  `DetailSKU` VARCHAR(50) NOT NULL,
  `DetailOrderID` INT(11) NOT NULL,
  `DetailProductID` INT(11) NOT NULL,
  PRIMARY KEY (`DetailID`),
  FOREIGN KEY (`DetailOrderID`) REFERENCES `ecommerce`.`orders` (`OrderID`),
  FOREIGN KEY (`DetailProductID`) REFERENCES `ecommerce`.`products` (`ProductID`));


